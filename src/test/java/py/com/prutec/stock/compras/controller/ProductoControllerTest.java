package py.com.prutec.stock.compras.controller;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import py.com.prutec.stock.compras.messaging.RabbitMQSender;
import py.com.prutec.stock.compras.model.Producto;
import py.com.prutec.stock.compras.service.impl.ProductoServiceImpl;

/**
 *
 * @author pbbattilana
 */
@ExtendWith(MockitoExtension.class)
public class ProductoControllerTest {

    @Mock
    private ProductoServiceImpl productoService;

    @Mock
    private RabbitMQSender rabbitMQSender;

    @InjectMocks
    private ProductoController productoController;

    @Test
    public void testCrearProducto() {
        Producto productoMock = new Producto(); // Crear un producto de ejemplo para la prueba

        when(productoService.crearProducto(productoMock)).thenReturn(productoMock); // Mockear el servicio

        ResponseEntity<Producto> response = productoController.crearProducto(productoMock); // Llamar al método del controlador

        assertEquals(HttpStatus.CREATED, response.getStatusCode()); // Verificar el código de estado
        assertEquals(productoMock, response.getBody()); // Verificar el cuerpo de la respuesta
    }

    @Test
    public void testComprarProducto() {
        Long idProducto = 1L;
        int cantidad = 5;

        ResponseEntity<String> response = productoController.comprarProducto(idProducto, cantidad); // Llamar al método del controlador

        assertEquals(HttpStatus.OK, response.getStatusCode()); // Verificar el código de estado
        assertEquals("Compra enviada con éxito (En proceso de actualización)", response.getBody()); // Verificar el cuerpo de la respuesta

        // Verificar que se llamó al método enviarMensajeCompra del RabbitMQSender con los parámetros correctos
        verify(rabbitMQSender).enviarMensajeCompra(idProducto, cantidad);
    }
}
