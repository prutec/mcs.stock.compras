package py.com.prutec.stock.compras.messaging;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.core.AmqpTemplate;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class RabbitMQSenderTest {

    @Mock
    private AmqpTemplate rabbitTemplate;

    @InjectMocks
    private RabbitMQSender rabbitMQSender;

    @Test
    public void testEnviarMensajeCompra() {
        Long idProducto = 1L;
        int cantidad = 5;

        rabbitMQSender.enviarMensajeCompra(idProducto, cantidad);

        // Verificar que se llamó a convertAndSend con los parámetros correctos
        verify(rabbitTemplate).convertAndSend("exchange.stock", "compras.stock.update", "1:5");
    }

    @Test
    public void testNotificarCompra() {
        Long idProducto = 1L;

        rabbitMQSender.notificarCompra(idProducto);

        // Verificar que se llamó a convertAndSend con los parámetros correctos
        verify(rabbitTemplate).convertAndSend("exchange.stock", "reprocesar.stock.update", "1");
    }
}
