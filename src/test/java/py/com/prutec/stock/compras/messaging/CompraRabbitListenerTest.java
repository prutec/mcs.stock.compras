package py.com.prutec.stock.compras.messaging;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.MessagePropertiesBuilder;
import py.com.prutec.stock.compras.service.impl.ProductoServiceImpl;

import static org.mockito.Mockito.verify;
import py.com.prutec.stock.compras.messaging.CompraRabbitListener;

@ExtendWith(MockitoExtension.class)
public class CompraRabbitListenerTest {

    @Mock
    private ProductoServiceImpl productoService;

    @InjectMocks
    private CompraRabbitListener compraRabbitListener;

    @Test
    public void testRealizarCompraAsync() {
        String mensaje = "1:5"; // Ejemplo de mensaje recibido desde RabbitMQ
        Message message = MessageBuilder.withBody(mensaje.getBytes())
                .andProperties(MessagePropertiesBuilder.newInstance()
                        .setContentType(MessageProperties.CONTENT_TYPE_TEXT_PLAIN)
                        .build())
                .build();

        compraRabbitListener.realizarCompraAsync(new String(message.getBody()));

        // Verificar que se llamó al método realizarCompra del productoService con los parámetros correctos
        verify(productoService).realizarCompra(1L, 5);
    }
}
