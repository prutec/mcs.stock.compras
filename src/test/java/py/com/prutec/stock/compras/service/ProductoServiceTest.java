package py.com.prutec.stock.compras.service;

import py.com.prutec.stock.compras.service.impl.ProductoServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import py.com.prutec.stock.compras.messaging.RabbitMQSender;
import py.com.prutec.stock.compras.model.Producto;
import py.com.prutec.stock.compras.repository.ProductoRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductoServiceTest {

    @Mock
    private ProductoRepository productoRepository;

    @Mock
    private RabbitMQSender rabbitMQSender;

    @InjectMocks
    private ProductoServiceImpl productoService;

    @Test
    public void testCrearProducto() {
        Producto producto = new Producto();
        producto.setId(1L);
        producto.setNombre("Producto Test");

        when(productoRepository.save(producto)).thenReturn(producto);

        Producto productoCreado = productoService.crearProducto(producto);

        assertNotNull(productoCreado);
        assertEquals("Producto Test", productoCreado.getNombre());
        assertNotNull(productoCreado.getFechaCreacion());
        assertNotNull(productoCreado.getFechaActualizacion());
    }

    @Test
    public void testVerificarStockDisponible() {
        Long idProducto = 1L;
        int cantidad = 5;

        Producto producto = new Producto();
        producto.setId(idProducto);
        producto.setStockDisponible(10);

        when(productoRepository.findById(idProducto)).thenReturn(Optional.of(producto));

        boolean stockDisponible = productoService.verificarStockDisponible(idProducto, cantidad);

        assertTrue(stockDisponible);
    }

    @Test
    public void testRealizarCompra() {
        Long idProducto = 1L;
        int cantidad = 5;

        Producto producto = new Producto();
        producto.setId(idProducto);
        producto.setStockDisponible(10);

        when(productoRepository.findById(idProducto)).thenReturn(Optional.of(producto));

        productoService.realizarCompra(idProducto, cantidad);

        verify(productoRepository, times(1)).findById(idProducto);
        verify(rabbitMQSender, times(1)).notificarCompra(idProducto);
        assertEquals(15, producto.getStockDisponible()); // Comprobación de que se ha actualizado el stock correctamente
    }
}
