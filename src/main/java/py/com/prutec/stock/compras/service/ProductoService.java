package py.com.prutec.stock.compras.service;

import py.com.prutec.stock.compras.model.Producto;

/**
 *
 * @author pbbattilana
 */
public interface ProductoService {

    public Producto crearProducto(Producto producto);

    public boolean verificarStockDisponible(Long idProducto, int cantidad);

    public void realizarCompra(Long idProducto, int cantidad);
}
