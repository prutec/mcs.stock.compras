package py.com.prutec.stock.compras.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import py.com.prutec.stock.compras.model.Producto;

/**
 *
 * @author pbbattilana
 */
@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long> {
    
}