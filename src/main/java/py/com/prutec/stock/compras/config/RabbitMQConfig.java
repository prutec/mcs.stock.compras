package py.com.prutec.stock.compras.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author pbbattilana
 */
@Configuration
public class RabbitMQConfig {

    public static final String EXCHANGE_NAME = "exchange.stock";
    public static final String QUEUE_NAME_COMPRA = "cola_compra";
    public static final String QUEUE_NAME_REPROCESAR = "cola_compra_reprocesar";

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(EXCHANGE_NAME);
    }

    @Bean
    public Queue compraQueue() {
        return new Queue(QUEUE_NAME_COMPRA);
    }

    @Bean
    public Queue reprocesarQueue() {
        return new Queue(QUEUE_NAME_REPROCESAR);
    }

    @Bean
    public Binding bindingCompra(Queue compraQueue, DirectExchange directExchange) {
        return BindingBuilder.bind(compraQueue).to(directExchange).with("compras.stock.update");
    }

    @Bean
    public Binding bindingReprocesar(Queue reprocesarQueue, DirectExchange directExchange) {
        return BindingBuilder.bind(reprocesarQueue).to(directExchange).with("reprocesar.stock.update");
    }
}
