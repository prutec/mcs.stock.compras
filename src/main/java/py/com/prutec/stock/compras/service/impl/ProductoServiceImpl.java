package py.com.prutec.stock.compras.service.impl;

import java.time.LocalDate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import py.com.prutec.stock.compras.messaging.RabbitMQSender;

import py.com.prutec.stock.compras.model.Producto;
import py.com.prutec.stock.compras.repository.ProductoRepository;
import py.com.prutec.stock.compras.service.ProductoService;

/**
 *
 * @author pbbattilana
 */
@Slf4j
@Service
public class ProductoServiceImpl implements ProductoService {

    @Autowired
    private ProductoRepository productoRepository;

    @Autowired
    private RabbitMQSender rabbitMQSender;

    @Transactional
    @Override
    public Producto crearProducto(Producto producto) {
        producto.setFechaCreacion(LocalDate.now());
        producto.setFechaActualizacion(LocalDate.now());
        return productoRepository.save(producto);
    }

    @Transactional
    @Override
    public boolean verificarStockDisponible(Long idProducto, int cantidad) {
        Producto producto = productoRepository.findById(idProducto).orElse(null);
        return producto != null && producto.getStockDisponible() >= cantidad;
    }

    @Transactional
    @Override
    public void realizarCompra(Long idProducto, int cantidad) {
        Producto producto = productoRepository.findById(idProducto).orElseThrow(() -> new IllegalArgumentException("Producto no encontrado"));
        producto.setStockDisponible(producto.getStockDisponible() + cantidad);
        producto.setFechaActualizacion(LocalDate.now());
        productoRepository.save(producto);
        log.info("El stock con id:{} ha sido abastecido", idProducto);
        log.info("Notificando la compra a RabbitMQ para verificación de ventas procesadas_con_error");
        rabbitMQSender.notificarCompra(idProducto);
    }
}
