package py.com.prutec.stock.compras.messaging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import py.com.prutec.stock.compras.service.impl.ProductoServiceImpl;

/**
 *
 * @author pbbattilana
 */
@Slf4j
@Component
public class CompraRabbitListener {

    @Autowired
    private ProductoServiceImpl productoService;

    @RabbitListener(queues = "cola_compra")
    public void realizarCompraAsync(String mensaje) {
        String[] partes = mensaje.split(":");
        Long idProducto = Long.parseLong(partes[0]);
        int cantidad = Integer.parseInt(partes[1]);
        log.info("Mensaje recibido de RabbitMQ para compra de producto id:{}", idProducto);
        productoService.realizarCompra(idProducto, cantidad);
    }
}