package py.com.prutec.stock.compras.messaging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author pbbattilana
 */
@Slf4j
@Component
public class RabbitMQSender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    private static final String EXCHANGE_NAME = "exchange.stock";
    private static final String ROUTING_KEY_COMPRA = "compras.stock.update";
    private static final String ROUTING_KEY_REPROCESAR = "reprocesar.stock.update";

    public void enviarMensajeCompra(Long idProducto, int cantidad) {
        String mensaje = idProducto + ":" + cantidad;
        rabbitTemplate.convertAndSend(EXCHANGE_NAME, ROUTING_KEY_COMPRA, mensaje);
        log.info("Mensaje enviado a RabbitMQ para venta de producto id:{}", idProducto);
    }

    public void notificarCompra(Long idProducto) {
        String mensaje = idProducto.toString();
        rabbitTemplate.convertAndSend(EXCHANGE_NAME, ROUTING_KEY_REPROCESAR, mensaje);
        log.info("Mensaje enviado a RabbitMQ para reprocesar detalles procesados_con_error con producto id:{}", idProducto);
    }
}

