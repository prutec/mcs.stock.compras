package py.com.prutec.stock.compras.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import py.com.prutec.stock.compras.messaging.RabbitMQSender;
import py.com.prutec.stock.compras.model.Producto;
import py.com.prutec.stock.compras.service.impl.ProductoServiceImpl;

/**
 *
 * @author pbbattilana
 */
@RestController
@RequestMapping("/productos")
public class ProductoController {
    @Autowired
    private ProductoServiceImpl productoService;
    
    @Autowired
    private RabbitMQSender rabbitMQSender;
    
    @PostMapping
    public ResponseEntity<Producto> crearProducto(@RequestBody Producto producto) {
        Producto productoCreado = productoService.crearProducto(producto);
        return ResponseEntity.status(HttpStatus.CREATED).body(productoCreado);
    }
    
    @PostMapping("/comprar/{idProducto}/{cantidad}")
    public ResponseEntity<String> comprarProducto(@PathVariable Long idProducto, @PathVariable int cantidad) {
//        boolean stockDisponible = productoService.verificarStockDisponible(idProducto, cantidad);
//        if (!stockDisponible) {
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Stock insuficiente para la compra");
//        }
        rabbitMQSender.enviarMensajeCompra(idProducto, cantidad);
        return ResponseEntity.status(HttpStatus.OK).body("Compra enviada con éxito (En proceso de actualización)");
    }
}
